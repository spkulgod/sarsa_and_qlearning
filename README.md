#### Contents:  

This project presents an approach of implementing an on-policy TD Policy Iteration algorithm : SARSA and an off-policy TD Policy Iteration algorithm: Q-learning for the Gym, Hill climbing environment. The discretization of the statespace is done so that it is suitable for planning using SARSA and Q-learning. The hyper-parameters are varied to obtain a set that guarantee convergence to the optimal action-value function. 

### Results:

###### Optimal policy obtained:
![Alt text](gifs/figure.png "optimal policy")

###### Result visualization 
![Alt text](gifs/gif1.gif "gif1")
![Alt text](gifs/gif2.gif "gif2")


