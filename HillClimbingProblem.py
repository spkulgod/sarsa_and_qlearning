import gym
from gym import spaces
from time import asctime
import numpy as np
import copy
import matplotlib.pyplot as plt

np.set_printoptions(threshold=np.inf,linewidth=300)
class Planner:

	'''
	Initialization of all necessary variables to generate a policy:
		discretized state space
		control space
		discount factor
		learning rate
		greedy probability (if applicable)
	'''
	def __init__(self,epsilon,gamma,alpha,stop,scale_v,scale_x,ep_len):
		self.epsilon = epsilon
		self.gamma = gamma
		self.alpha = alpha
		self.stop_it = stop 
		self.scale_v = scale_v
		self.scale_x = scale_x
		self.ep_len = ep_len 
		self.scale = np.array([self.scale_x,self.scale_v])
		v_num = int(np.round(2*0.07/self.scale_v+1))
		x_num = int(np.round((0.6+1.2)/self.scale_x+1))
		print(v_num,x_num)
		self.min = np.array([-1.2,-0.07])
		self.U = [0,1,2]
		self.Q = np.zeros((x_num,v_num,3))
		self.a = np.round((np.array([0,0])-self.min)/self.scale)
		self.b = np.round((np.array([-1,0.035])-self.min)/self.scale)
		self.c = np.round((np.array([0.2,-0.025])-self.min)/self.scale)
		self.a=self.a.astype(int)
		self.b=self.b.astype(int)
		self.c=self.c.astype(int)
		print(self.b)
		print(self.c)

	'''
	Learn and return a policy via model-free policy iteration.
	'''
	def __call__(self, mc=False, on=True):
		return self._td_policy_iter(on)

	def epsilon_greedy(self,u):
		rand = np.random.random_sample()
		e_u = self.epsilon/3
		if rand<=(1-self.epsilon+e_u):
			return u
		elif rand<=(1-self.epsilon+2*e_u):
			return (u+1)%3
		return (u+2)%3

	def generate_episode(self,pi):
		c_state = np.array(env.reset())
		done = False
		u = []
		x = []
		l = []
		while not done:
			pos_grid = np.round((c_state-self.min)/self.scale)
			action = self.epsilon_greedy(pi[tuple(pos_grid.astype(int))])
			x.append(pos_grid.astype(int))
			u.append(action)
			c_state, reward, done, _ = env.step(action)
			#print(c_state)
			l.append(-reward)
		#print(np.shape(l))
		return x,u,l

	'''
	TO BE IMPLEMENT
	TD Policy Iteration
	Flags: on : on vs. off policy learning
	Returns: policy that minimizes Q wrt to controls
	'''
	def _td_policy_iter(self, on=True):
		Q_new = copy.deepcopy(self.Q)
		Q = np.ones(np.shape(Q_new))
		length = self.ep_len
		count = 0
		Qa = []
		Qb = []
		Qc = []
		#N_x = np.ones(np.shape(Q_new)[:-1])
		if on:
			while np.max(abs(Q-Q_new))>self.stop_it or length==self.ep_len:
				count+=1
				Q  = copy.deepcopy(Q_new)
				pi = np.argmin(Q,axis=2)
				x,u,l = self.generate_episode(pi)
				length = len(l)
				t = 0
				while t<length-2:
					t+=1
					Q_new[x[t][0],x[t][1],u[t]] = Q_new[x[t][0],x[t][1],u[t]]+\
						self.alpha*(l[t]+\
						self.gamma*Q_new[x[t+1][0],x[t+1][1],u[t+1]]\
						-Q_new[x[t][0],x[t][1],u[t]])
				if count%1 == 0:
					Qa.append([Q_new[self.a[0],self.a[1],0],
						Q_new[self.a[0],self.a[1],1],
						Q_new[self.a[0],self.a[1],2]])
					Qb.append([Q_new[self.b[0],self.b[1],0],
						Q_new[self.b[0],self.b[1],1],
						Q_new[self.b[0],self.b[1],2]])
					Qc.append([Q_new[self.c[0],self.c[1],0],
						Q_new[self.c[0],self.c[1],1],
						Q_new[self.c[0],self.c[1],2]])
			Qa = np.array(Qa)
			Qb = np.array(Qb)
			Qc = np.array(Qc)
			np.save('on_policy/Qa',Qa)
			np.save('on_policy/Qb',Qb)
			np.save('on_policy/Qc',Qc)
			for i in range(3):
				plt.clf()
				plt.plot(Qa[:,i])
				plt.xlabel("Episode number")
				plt.ylabel("Value")
				plt.savefig('Qa -op'+str(i)+'.png')
				plt.clf()
				plt.plot(Qb[:,i])
				plt.xlabel("Episode number")
				plt.ylabel("Value")
				plt.savefig('Qb-op'+str(i)+'.png')
				plt.clf()
				plt.plot(Qc[:,i])
				plt.xlabel("Episode number")
				plt.ylabel("Value")
				plt.savefig('Qc-op'+str(i)+'.png')

		else :
			while np.max(abs(Q-Q_new))>self.stop_it or length==self.ep_len:
				count+=1
				Q  = copy.deepcopy(Q_new)
				pi = np.argmin(Q,axis=2)
				x,u,l = self.generate_episode(pi)
				length = len(l)
				t = 0
				while t<length-2:
					t+=1
					Q_new[x[t][0],x[t][1],u[t]] = Q_new[x[t][0],x[t][1],u[t]]+\
						self.alpha*(l[t]+self.gamma*\
						np.min(Q_new[x[t+1][0],x[t+1][1],:])\
						-Q_new[x[t][0],x[t][1],u[t]])
				if count%1 == 0:
					Qa.append([Q_new[self.a[0],self.a[1],0],
						Q_new[self.a[0],self.a[1],1],
						Q_new[self.a[0],self.a[1],2]])
					Qb.append([Q_new[self.b[0],self.b[1],0],
						Q_new[self.b[0],self.b[1],1],
						Q_new[self.b[0],self.b[1],2]])
					Qc.append([Q_new[self.c[0],self.c[1],0],
						Q_new[self.c[0],self.c[1],1],
						Q_new[self.c[0],self.c[1],2]])
			Qa = np.array(Qa)
			Qb = np.array(Qb)
			Qc = np.array(Qc)
			np.save('off_policy/Qa',Qa)
			np.save('off_policy/Qb',Qb)
			np.save('off_policy/Qc',Qc)
			for i in range(3):
				plt.clf()
				plt.plot(Qa[:,i])
				plt.xlabel("Episode number")
				plt.ylabel("Value")
				plt.savefig('Qa-off'+str(i)+'.png')
				plt.clf()
				plt.plot(Qb[:,i])
				plt.xlabel("Episode number")
				plt.ylabel("Value")
				plt.savefig('Qb-off'+str(i)+'.png')
				plt.clf()
				plt.plot(Qc[:,i])
				plt.xlabel("Episode number")
				plt.ylabel("Value")
				plt.savefig('Qc-off'+str(i)+'.png')
		self.policy = pi
		self.Q = Q_new




	'''
	Sample trajectory based on a policy
	'''
	def rollout(self, env, policy=None, render=False):
		traj = []
		t = 0
		done = False
		c_state = env.reset()
		if policy is None:
			while not done and t < 200:
				action = env.action_space.sample()
				if render:
					env.render()
				n_state, reward, done, _ = env.step(action)
				traj.append((c_state, action, reward))
				c_state = n_state
				t += 1

			env.close()
			return traj

		else:
			while not done and t < 200:
				pos_grid = np.round((c_state-self.min)/self.scale)
				action = policy[tuple(pos_grid.astype(int))]
				if render:
					env.render()
				n_state, reward, done, _ = env.step(action)
				traj.append((c_state, action, reward))
				c_state = n_state
				t += 1
			print(t)
			env.close()
			return traj


if __name__ == '__main__':
	epi_len=1000
	gym.envs.register(
    	id='MountainCarMyEasyVersion-v0',
    	entry_point='gym.envs.classic_control:MountainCarEnv',
    	max_episode_steps=epi_len,      # MountainCar-v0 uses 200
    	reward_threshold=-110.0,
	)
	env = gym.make('MountainCarMyEasyVersion-v0')
	#env = gym.make('MountainCar-v0')
	planner = Planner(epsilon=0.15,gamma=0.9,alpha=0.1,stop=0.005,scale_v=0.002\
			,scale_x=0.02,ep_len=epi_len)
	#planner = Planner(epsilon=0.2,gamma=0.9,alpha=0.1,stop=0.01,scale_v=0.005\
	#		,scale_x=0.05,ep_len=epi_len)
	planner(on=True)
	#planner.policy = np.load('on_policy_policy.npy')
	print(planner.policy)
	np.save('on_policy_policy',planner.policy)
	plt.imshow(planner.policy,aspect='auto',extent=[-0.07,0.07,0.6,-1.2])
	plt.xlabel("Velocity")
	plt.ylabel("X-Position")
	plt.colorbar()
	plt.show()
	env = gym.wrappers.Monitor(env, './videos/sut'+asctime(),force = True)
	traj = planner.rollout(env, planner.policy, render=True)
	#print(traj)

